package com.example.realtimeconversor.presentation.base.common

import com.example.movo.domain.di.component.ScreenComponent

interface DaggerController {

    val screen: ScreenComponent

}