package com.example.realtimeconversor.presentation.base

import android.support.v7.app.AppCompatActivity
import com.example.movo.domain.di.component.ScreenComponent
import com.example.movo.domain.di.module.ScreenModule
import com.example.realtimeconversor.RealTimeConversorApp
import com.example.realtimeconversor.domain.presenter.Presenter
import com.example.realtimeconversor.domain.presenter.PresenterView
import com.example.realtimeconversor.presentation.base.common.DaggerController


abstract class BaseActivity : AppCompatActivity(), DaggerController {

    private var presenter: Presenter<*>? = null

    fun <T : PresenterView> init(presenter: Presenter<T>, view: T) {
        this.presenter = presenter
        presenter.init(view)
    }

    override val screen: ScreenComponent by lazy {
        DaggerScreenComponent.builder()
            .appComponent(RealTimeConversorApp.appController.appComponent)
            .screenModule(ScreenModule(this))
            .build()
    }

    override fun onDestroy() {
        presenter?.clear()
        super.onDestroy()
    }

    fun close() {
        finish()
    }
}