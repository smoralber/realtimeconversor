package com.example.realtimeconversor

import android.app.Application
import android.content.Context
import com.example.movo.domain.di.component.AppComponent
import com.example.movo.domain.di.module.AppModule
import com.example.realtimeconversor.domain.di.AppController

class RealTimeConversorApp : Application(), AppController {

    companion object {

        lateinit var appController: AppController
    }

    override val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
    }

    override fun onCreate() {
        super.onCreate()

        appController = this

    }
}