package com.example.movo.domain.di.module

import android.content.Context
import android.content.res.Resources
import com.example.movo.MovoApp
import com.example.movo.domain.di.scope.AppScope
import dagger.Module
import dagger.Provides

@Module(
    includes = [ClientModule::class,
        ServiceModule::class,
        UseCaseModule::class]
)
class AppModule(private val app: MovoApp) {

    @Provides
    @AppScope
    fun context(): Context = app.applicationContext

    @Provides
    @AppScope
    fun resources(): Resources = app.resources

}