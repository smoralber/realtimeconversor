package com.example.realtimeconversor.domain.di

import com.example.movo.domain.di.component.AppComponent

interface AppController {

    val appComponent: AppComponent
}