package com.example.movo.domain.di.scope

import javax.inject.Scope

@Scope
@kotlin.annotation.Retention
annotation class AppScope