package com.example.movo.domain.di.module

import com.example.movo.presentation.base.BaseActivity
import dagger.Module

@Module(includes = [UseCaseModule::class])
class ScreenModule(private val activity: BaseActivity) {

}