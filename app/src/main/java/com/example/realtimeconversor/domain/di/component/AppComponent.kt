package com.example.movo.domain.di.component

import android.content.Context
import android.content.res.Resources
import com.example.movo.domain.di.module.AppModule
import dagger.Component

@Component(modules = [AppModule::class])
interface AppComponent {

    fun provideResources(): Resources
    fun provideContext(): Context
}