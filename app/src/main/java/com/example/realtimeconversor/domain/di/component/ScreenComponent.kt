package com.example.movo.domain.di.component

import com.example.movo.domain.di.module.ScreenModule
import com.example.movo.presentation.SplashScreenActivity
import dagger.Component

@Component(dependencies = [AppComponent::class], modules = [ScreenModule::class])
interface ScreenComponent {

    fun inject(splashScreenActivity: SplashScreenActivity)
}