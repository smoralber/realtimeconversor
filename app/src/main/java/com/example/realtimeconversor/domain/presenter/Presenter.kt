package com.example.realtimeconversor.domain.presenter

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class Presenter<V : PresenterView> {

    var view: V? = null
    private lateinit var compositeDisposable: CompositeDisposable

    protected abstract fun init()

    fun init(presenterView: V) {
        this.view = presenterView
        compositeDisposable = CompositeDisposable()
        init()
    }

    fun clear() {
        this.view = null
        compositeDisposable.dispose()
    }

    protected fun addSubscription(disposable: Disposable) =
        compositeDisposable.add(disposable)
}